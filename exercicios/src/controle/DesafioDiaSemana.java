/**
 * 
 */
package controle;

import java.util.Scanner;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class DesafioDiaSemana {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Que dia é hoje?");
		String diaDaSemana = scanner.next();

		int dia = 0;

		if (diaDaSemana.equals("domingo")) {
			dia = 1;
		} else if (diaDaSemana.equalsIgnoreCase("segunda")) {
			dia = 2;
		} else if (diaDaSemana.equalsIgnoreCase("terça") || diaDaSemana.equalsIgnoreCase("terca")) {
			dia = 3;
		} else if (diaDaSemana.equalsIgnoreCase("quarta")) {
			dia = 4;
		} else if (diaDaSemana.equalsIgnoreCase("quinta")) {
			dia = 5;
		} else if (diaDaSemana.equalsIgnoreCase("sexta")) {
			dia = 6;
		} else if (diaDaSemana.equalsIgnoreCase("sábado") || diaDaSemana.equalsIgnoreCase("sabado")) {
			dia = 7;
		}

		if (dia == 0) {
			System.out.println("Dia inválido");
		} else {
			System.out.printf("Hoje é o dia %d", dia);
		}

		scanner.close();

	}

}
