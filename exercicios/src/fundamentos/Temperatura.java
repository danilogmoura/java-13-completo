/**
 * 
 */
package fundamentos;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class Temperatura {

	public static void main(String[] args) {
		final double FATOR = 5.0 / 9.0;
		final int AJUSTE = 32;

		double fahrenheit = 86;
		double celsius = (fahrenheit - AJUSTE) * FATOR;
		System.out.println("Temperatura: " + celsius + "°C");

		fahrenheit = 150;
		celsius = (fahrenheit - AJUSTE) * FATOR;
		System.out.println("Temperatura: " + celsius + "°C");
	}
}
