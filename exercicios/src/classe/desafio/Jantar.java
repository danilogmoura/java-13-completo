package classe.desafio;

public class Jantar {

    public static void main(String[] args) {

        Comida feijao = new Comida("Feijão", 0.223);
        Comida arroz = new Comida("Arroz", 0.3);
        Comida carne = new Comida("Carne", 0.8);

        Pessoa danilo = new Pessoa("Danilo", 97.12);
        System.out.println(danilo.apresentar());

        danilo.comer(feijao);
        System.out.println(danilo.apresentar());

        danilo.comer(arroz);
        System.out.println(danilo.apresentar());

        danilo.comer(carne);
        System.out.println(danilo.apresentar());
    }
}
