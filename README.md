# Java 13 COMPLETO

Fundamentos Java, Orientação a Objeto, Programação Funcional, MySQL, MongoDB, Spring Boot, JavaFX, JPA, Hibernate e mais

https://www.udemy.com/course/fundamentos-de-programacao-com-java/

O curso de Java 13 foi completamente refeito para se tornar o conteúdo de Java mais atualizado e completo da língua portuguesa! Neste curso você aprenderá Java do absoluto zero até se tornar um programador profissional, dominando os recursos mais moderno da linguagem. Alguns dos assuntos abordados:

Algoritmo e Estrutura de Dados

Fundamentos da Linguagem Java

Estruturas de Controle

Classes, Objetos, Métodos

Orientação a Objeto

Encapsulamento, Herança, Polimorfismo e Abstração

Lambdas

Stream API

Tratamento de Exceções

JavaFX

Banco de Dados Relacional

Bando de Dados NÃO Relacional (NoSQL)

JPA (Hibernate)

Spring Boot