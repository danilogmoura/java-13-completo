/**
 * 
 */
package classe;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class DataTeste {
    public static void main(String[] args) {

	Data data1 = new Data();

//		data1.dia = 30;
//		data1.mes = 04;
//		data1.ano = 1987;

	System.out.println(data1.obterDataFormatada());
	data1.imprimirDataFormatada();

	Data data2 = new Data(31, 12, 2020);

//		data2.dia = 31;
//		data2.mes = 12;
//		data2.ano = 2020;

	System.out.println(data2.obterDataFormatada());
	data2.imprimirDataFormatada();
    }

}
