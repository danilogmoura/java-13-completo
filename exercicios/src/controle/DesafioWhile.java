/**
 * 
 */
package controle;

import java.util.Scanner;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class DesafioWhile {

	public static void main(String[] args) {

		double total = 0;
		int notasValidas = 0;

		boolean calcular = true;

		Scanner scanner = new Scanner(System.in);

		double notaAtual = 0;
		do {
			System.out.print("Digite a nota: ");
			notaAtual = scanner.nextDouble();

			if (notaAtual == -1) {
				calcular = false;
			} else if (notaAtual >= 0 && notaAtual <= 10) {
				total += notaAtual;
				notasValidas++;
			} else {
				System.out.println("Nota inválida! Digite uma nota válida por favor!");
			}

		} while (calcular);

		scanner.close();

		System.out.printf("Tivemos no total %d alunos e a média da sala é %.2f", notasValidas, (total / notasValidas));
	}
}
