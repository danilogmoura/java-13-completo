/**
 * 
 */
package controle;

import java.util.Scanner;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class WhileIndeterminado {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		String palavra = "";

		while (!"sair".equalsIgnoreCase(palavra)) {
			System.out.print("Você diz: ");
			palavra = scanner.nextLine();
		}

		scanner.close();
	}
}
