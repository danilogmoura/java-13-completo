/**
 * 
 */
package fundamentos;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class NotacaoPonto {

	public static void main(String[] args) {
		String s = "Bom dia X";
		s = s.replace("X", "Senhora");
		s = s.toUpperCase();
		s = s.concat("!!!");

		System.out.println(s);

		String d = "danilo".toUpperCase();
		System.out.println(d);

		String y = "Bom dia X".replace("X", "Danilo").toUpperCase().concat("!!!");
		System.out.println(y);

		// Tipos primitivos não tem o operador "."
		int a = 3;
		// a.
		System.out.println(a);
	}

}
