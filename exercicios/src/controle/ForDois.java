/**
 * 
 */
package controle;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class ForDois {

	public static void main(String[] args) {

		for (int contador = 10; contador >= 0; contador -= 2) {
			System.out.println(contador);
		}
	}

}
