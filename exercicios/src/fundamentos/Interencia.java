/**
 * 
 */
package fundamentos;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class Interencia {

	public static void main(String[] args) {

		double a = 4.5;
		System.out.println(a);

		double  b = 4.5;
		System.out.println(b);

		String c = "Texto";
		System.out.println(c);

		c = "Outro texto";
		System.out.println(c);

		double d; // variável foi declarada
		d = 123.65; // variável foi inicializada
		System.out.println(d); // usada!

		double e = 123.45;
		System.out.println(e);
	}
}
