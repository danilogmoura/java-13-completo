/**
 * 
 */
package controle;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class ForUm {
	public static void main(String[] args) {

		for (int contador = 1; contador <= 10; contador++) {
			System.out.printf("Bom dia! i = %d\n", contador);
		}

		// Laço infinito!!!
//		for (;;) {
//			System.out.println("Fim!");
//		}
	}
}
