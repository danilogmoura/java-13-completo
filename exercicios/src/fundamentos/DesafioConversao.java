/**
 * 
 */
package fundamentos;

import java.util.Scanner;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class DesafioConversao {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.printf("Digite o primeiro salário: ");
		String entrada1 = scanner.next().replace(",", ".");
		double salario1 = Double.parseDouble(entrada1);

		System.out.printf("Digite o segundo salário: ");
		String entrada2 = scanner.next().replace(",", ".");
		double salario2 = Double.parseDouble(entrada2);

		System.out.printf("Digite o terceiro salário: ");
		String entrada3 = scanner.next().replace(",", ".");
		double salario3 = Double.parseDouble(entrada3);

		double soma = salario1 + salario2 + salario3;

		System.out.println("A média dos três ultimos salários é: " + soma / 3);

		scanner.close();
	}
}
