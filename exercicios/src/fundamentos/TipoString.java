/**
 * 
 */
package fundamentos;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class TipoString {

	public static void main(String[] args) {
		System.out.println("Olá pessoal".charAt(2));

		String s = "Boa tarde";
		System.out.println(s.concat("!!!"));
		System.out.println(s + "!!!");
		System.out.println(s.startsWith("Boa"));
		System.out.println(s.toLowerCase().startsWith("boa"));
		System.out.println(s.endsWith("tarde"));
		System.out.println(s.toUpperCase().endsWith("TARDE"));
		System.out.println(s.equalsIgnoreCase("boa tarde"));

		String nome = "Pedro";
		String sobremone = "Santos";
		double idade = 33;
		double salario = 12_234.987;

		System.out.println(
				"Nome: " + nome + "\nSobrenome: " + sobremone + "\nIdade: " + idade + "\nSalário: " + salario + "\n\n");

		String frase = String.format("\nO senhos %s %s tem %d anos e ganha R$%.2f", nome, sobremone, idade, salario);
		System.out.println(frase);

		System.out.println("Frase qualquer".contains("qual"));
		System.out.println("Frase qualquer".indexOf("qual"));
		System.out.println("Frase qualquer".substring(6));
		System.out.println("Frase qualquer".substring(6, 8));
	}
}
