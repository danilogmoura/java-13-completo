/**
 * 
 */
package fundamentos;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class ConversaoTiposPrimitivosNumerico {

	public static void main(String[] args) {

		double a = 1; // implícita
		System.out.println(a);

		float b = (float) 1.1235451456456; // explícita (cast)
		System.out.println(b);

		int c = 4;
		byte d = (byte) c; // explícita (cast)
		System.out.println(d);

		double e = 1.999999;
		int f = (int) e; // explícita (cast)
		System.out.println(f);
	}
}
