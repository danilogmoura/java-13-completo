/**
 * 
 */
package operadores;

import java.util.Scanner;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class DesfioCalculadora {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Informe o número: ");
		double num1 = scanner.nextDouble();

		System.out.println("Informe o número: ");
		double num2 = scanner.nextDouble();

		System.out.println("Informe a operação: ");
		String operacao = scanner.next();

		// Lógica
		double resultado = "+".equals(operacao) ? num1 + num2 : 0;
		resultado = "-".equals(operacao) ? num1 - num2 : resultado;
		resultado = "*".equals(operacao) ? num1 * num2 : resultado;
		resultado = "/".equals(operacao) ? num1 / num2 : resultado;
		resultado = "%".equals(operacao) ? num1 % num2 : resultado;

		System.out.printf("%.2f %s %.2f = %.2f", num1, operacao, num2, resultado);

		scanner.close();
	}

}
