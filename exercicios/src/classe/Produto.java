/**
 * 
 */
package classe;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class Produto {

    String nome;
    double preco;
    static double desconto = 0.25;

    /**
     * 
     */
    Produto(String nomeIncial, double precoInicial) {
        nome = nomeIncial;
        preco = precoInicial;
    }

    /**
     * 
     */
    Produto() {
        // TODO Auto-generated constructor stub
    }

    double precoComDesconto() {
        return preco * (1 - desconto);
    }

    double precoComDesconto(double descontoDoGerente) {
        return preco * (1 - desconto + descontoDoGerente);
    }
}
