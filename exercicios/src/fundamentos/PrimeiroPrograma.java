package fundamentos;


/**
 * Essa classe representa...
 * 
 * @author Danilo Gomes de Moura <danilogmoura@gmail.com>
 * @since JDK1.0
 * @see
 *
 */
public class PrimeiroPrograma {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		// Uma senteça de códico termina com ;		
		// Mais um comentário...
		// Fim
		System.out.println("Primeiro Programa Parte #01!");
		
		/**
		 * Linha 1
		 * Linha 2
		 * Linha 3
		 */
		System.out.println("Primeiro Programa Parte #02!");
		
		System.out.println("Fim"); // Aqui também funciona
	}
}
