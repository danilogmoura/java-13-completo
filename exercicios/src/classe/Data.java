/**
 *
 */
package classe;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class Data {

    int dia;
    int mes;
    int ano;

    Data() {
//        dia = 1;
//        mes = 1;
//        ano = 1970;
        this(1, 1, 1970);
    }

    /**
     *
     * @param dia
     * @param mes
     * @param ano
     */
    Data(int dia, int mes, int ano) {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }

    String obterDataFormatada() {
        final String formato = "%d/%2d/%d";
        return String.format(formato, this.dia, this.mes, this.ano);
    }

    void imprimirDataFormatada() {
        System.out.println(this.obterDataFormatada());
    }

//    static void teste(){
//        this.dia;
//    }
}
