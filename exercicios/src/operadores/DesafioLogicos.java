/**
 * 
 */
package operadores;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class DesafioLogicos {
	public static void main(String[] args) {

		// Travbalho na terça (V ou F)
		// Travbalho na quinta (V ou F)

		boolean trabalho1 = true;
		boolean trabalho2 = true;

		boolean comprouTV50 = trabalho1 && trabalho2;
		boolean comprouTV32 = trabalho1 ^ trabalho2;
		boolean comprouSorvete = trabalho1 || trabalho2;

		// Operador unário
		boolean maisSaudavel = !comprouSorvete;

		System.out.println("Comprou TV 50\"? " + comprouTV50);
		System.out.println("Comprou TV 50\"? " + comprouTV32);
		System.out.println("Comprou TV 50\"Sorvete ? " + comprouSorvete);
		System.out.println("Mais saudável ? " + maisSaudavel);
	}
}
