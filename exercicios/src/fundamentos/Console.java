/**
 * 
 */
package fundamentos;

import java.util.Scanner;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class Console {

	public static void main(String[] args) {

		System.out.print("Bom");
		System.out.print(" dia!\n\n");

		System.out.println("Bom");
		System.out.println("dia!");

		System.out.printf("MEgasena %d %d %d %d %d %d", 1, 2, 3, 4, 5, 6);

		System.out.printf("Salário: %.1f%n", 1_234.4_567);
		System.out.printf("Nome: %s%n", "João");

		Scanner entrada = new Scanner(System.in);

		System.out.print("Digite o seu nome: ");
		String nome = entrada.nextLine();

		System.out.print("Digite o seu sobrenome: ");
		String sobrenome = entrada.nextLine();

		System.out.print("Digite o sua idade: ");
		int idade = entrada.nextInt();

		System.out.printf("%s %s tem %d anos.%n", nome, sobrenome, idade);

		entrada.close();
	}
}
