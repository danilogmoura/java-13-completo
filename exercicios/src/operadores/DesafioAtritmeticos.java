/**
 * 
 */
package operadores;

/**
 * @author Danilo Gomes de Moura
 *
 */
public class DesafioAtritmeticos {

	public static void main(String[] args) {

		double left = Math.pow(6 * (3 + 2), 2);
		left = left / (3 * 2);

		double right = ((1 - 5) * (2 - 7)) / 2;
		right = Math.pow(right, 2);

		double left_ritgh = left - right;

		double result = Math.pow(left_ritgh, 3) / Math.pow(10, 3);
		System.out.println("O resultado é: " + result);

		// segunda

		double numA = Math.pow(6 * (3 + 2), 2);
		double denA = 3 * 2;

		double numB = (1 - 5) * (2 - 7);
		double denB = 2;

		double superiorA = numA / denA;
		double superiorB = Math.pow(numB / denB, 2);

		double superior = Math.pow(superiorA - superiorB, 3);
		double inferior = Math.pow(10, 3);

		double resultado = superior / inferior;
		System.out.println("O resultado é: " + resultado);
	}

}
